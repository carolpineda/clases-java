package Dia6;

public class Constructores2 {
    public String metodo2(Double num1, Double num2){
        return num1.toString() + "\n" + num2.toString() + "\n";
    }

    public static void main(String[] args){
        Constructores2 cons2 = new Constructores2();
        Constructores cons3 = new Constructores();
        System.out.println(cons2.metodo2(10.5, 20.6) + cons3.metodo("Carolina", "Pineda", 15, 30));
    }
}
