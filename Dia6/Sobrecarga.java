package Dia6;

public class Sobrecarga {
    private String nombre;
    private String apellido;
    private Integer edad;
    private Double estatura;
    
    public String animales(String perro, Integer num3){
        return perro + num3;
    }

    public String animales(Double num, String gato, Integer num2){
        return num + gato + num2;
    }

    public Sobrecarga(String nombre, String apellido, Integer edad){
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }

    public Sobrecarga(String nombre, String apellido, Integer edad, Double estatura){
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.estatura = estatura;
    }

    public void Colores(String ojos, String arbol, Integer manos){
        System.out.println("El arbol tiene: " + ojos + manos + arbol);
    }

    public void Colores(String ojos, String arbol, Integer manos, Integer pies){
        System.out.println("El arbol tiene: " + ojos + manos + pies + arbol);
    }
}
