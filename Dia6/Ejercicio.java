package Dia6;
import java.util.ArrayList;
import java.util.List;

public class Ejercicio {
    //10, 58, 68, 920, 15, 16, 85, 75, 32
    //cambiar 68 con index 2 por numero 90
    //cambiar 920 con index 3 por numero 25
    //Imprimir los valores de la lista y su index


    public static void main(String[] args){
        //ArrayList ejercicio = new ArrayList<>();
        
        List<Integer> miLista = new ArrayList<Integer>(){{add(10); add(58); add(68); add(920); add(15); add(16); add(85); add(75); add(32);}};
        System.out.println(miLista);

        //miLista.remove(2);
        //miLista.remove(2);
        //System.out.println(miLista);

        miLista.set(2, 90);
        miLista.set(3, 25);
        System.out.println("Lista nueva: "+ miLista);

        for(int i = 0; i < miLista.size(); i ++ ){
            System.out.println("Index: " + i + " Valor: " + miLista.get(i));
        }
       
    }
}
