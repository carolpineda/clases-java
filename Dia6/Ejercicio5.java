package Dia6;
import java.util.ArrayList;

public class Ejercicio5 {
    
public static void main(String[] args){
    ArrayList <String> ejercicio5 = new ArrayList<>();
    ejercicio5.add("15");
    ejercicio5.add("30");
    ejercicio5.add("12");
    ejercicio5.add("60");
    
    System.out.println(ejercicio5);

    Integer con = 0;
    for(String num:ejercicio5 ){
        Integer nuevo = Integer.valueOf(num);
        con = con + nuevo;
    }

    System.out.println("La suma total es: " + con);
}
}
