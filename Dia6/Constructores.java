package Dia6;

public class Constructores {
    
    public String metodo(String verde, String rojo, Integer peras, Integer manzanas){
        return verde + "\n" + rojo + "\n" + peras.toString() + "\n" + manzanas.toString();   
    }

    
    public static void main(String[] args) {
        Constructores cons = new Constructores();
        System.out.println(cons.metodo("Hola", "Adios", 4, 5));
        
    }
}
