package Dia7y8;

import java.util.Collections;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Arrays;


public class Numeros {
    public static void main(String[] args) {
        
        // Ponemos nuestra ArrayList y el tipo de esta misma.
        ArrayList <Integer> numlist = new ArrayList<>();
        // Colocamos nuestro Scanner para poder pedirle datos al usuarios.
        Scanner numeros = new Scanner(System.in);
        System.out.println("Introduce cualquier numero entero: ");  
        
        // Hacemos un bucle while con una variable nueva para que solo se tenga una estrada de datos. 
        Integer cont_pri = 0;
        boolean primo;
        Integer j;
        Integer negativo = 0;
        Integer positivo = 0;
        int uno;
        while((uno = numeros.nextInt()) != -1){
            numlist.add(uno);
            
            if(uno >= 0){
                positivo = positivo + uno;
                // System.out.println("Suma numeros positivos: " + positivo);
            }
            if(uno <= 0){  
                negativo = negativo + uno;
                // System.out.println("Suma numeros negativos: " + negativo);
            }
        }
        
        
        numeros.close();
        
        //Se imprime todos los numeros ingresados por el usuario.
        System.out.println("Los numeros ingresados son: " + numlist);

        

        //Se saca el numero mayor de nuestra ArrayList y se imprime.
        System.out.println("El numero mayor es: " + Collections.max(numlist));

        //Se saca el numero menor de nuestra ArrayList y se imprime.
        System.out.println("El numero menor es: " + Collections.min(numlist));

        //Suma de nuestra ArrayList.
        int suma = numlist.stream().reduce(Integer::sum).get();
        System.out.println("La suma de todos los numeros ingresados es: " + suma);

        // Suma de nuestra ArrayList de otra manera.
        int c=0;
        for(int a:numlist){
            c = c+a;
        }
        ArrayList <Integer> lista = new ArrayList<>();
        lista.add(c);
        System.out.println("La suma total es: " + Collections.max(lista));

        // Sacar numeros primos de nuestra ArrayList.
        for(int i=1;i<=Collections.max(lista);i++){
            primo=true;
            j=2;
            while (j<=i-1 && primo==true)
        {
            if (i%j==0)
            primo=false;
            j++;
        }
            if (primo==true){
            cont_pri++; // si es primo incrementamos el contador de primos
            System.out.println(i+(" es primo"));
        }
    }

        System.out.println("Suma numeros positivos: " + positivo);
        System.out.println("Suma numeros negativos: " + negativo);
        
        System.out.println("En el rango 1.." + Collections.max(lista) + ", hay "+ cont_pri + " números primos");
        
            
        }
    }

        

        
        
      
