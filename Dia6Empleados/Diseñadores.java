package Dia6Empleados;
import java.util.ArrayList;
import java.util.Collections;


public class Diseñadores extends Empleados implements Metodo_act_diarias{
    private String herramienta_diseño;
    private ArrayList<String>actividades_diarias_empleado = new ArrayList<>();
    private Integer codigo_diseñador;

    public Diseñadores(String nombre, String apellido, Integer edad, Integer codigo, String herramienta){
        super(nombre, apellido, edad);
        setCodigo_diseñador(8000);
        setHerramienta_diseño("Photoshop");

        actividades_diarias_empleado.add("Reuniones de avance");
        actividades_diarias_empleado.add("Precaucion-ajustes del diseño");
        actividades_diarias_empleado.add("Elaboracion de diseño para paginas web");

        if(edad <=25){
           actividades_diarias_empleado.add("Revision de diseñador SR"); 
        } else {
            actividades_diarias_empleado.add("Apoyo a diseñador JR");
        }
    }

    public String getHerramienta_diseño() {
        return herramienta_diseño;
    }

    public Integer getCodigo_diseñador() {
        return codigo_diseñador;
    }

    public void setHerramienta_diseño(String herramienta_diseño) {
        this.herramienta_diseño = herramienta_diseño;
    }

    public void setCodigo_diseñador(Integer codigo_diseñador) {
        this.codigo_diseñador = codigo_diseñador;
    }
    
    @Override
    public void obtener_actividades(){
        Collections.sort(actividades_diarias_empleado);
        for(String abc:actividades_diarias_empleado){
            System.out.println(abc);
        }
    }
    
}
