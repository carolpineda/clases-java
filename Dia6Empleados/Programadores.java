package Dia6Empleados;
import java.util.ArrayList;
import java.util.Collections;

public class Programadores extends Empleados implements Metodo_act_diarias {
    private String herramienta_desarrollo;
    private ArrayList<String> actividades_diarias = new ArrayList<>();
    private Integer codigo_programador;

    public Programadores(String nombre, String apellido, Integer edad, String herramienta, Integer codigo){
        super(nombre, apellido, edad);
        setHerramienta_desarrollo("Eclipse");
        setCodigo_programador(9000);

        actividades_diarias.add("Analisis de requerimientos");
        actividades_diarias.add("Desarrollo de requerimientos");
        actividades_diarias.add("Presentacion-pruebas unitarias");
    
    }

        
    public void setHerramienta_desarrollo(String herramienta){
        this.herramienta_desarrollo = herramienta;
    }

    public String getHerramienta_desarrollo(){
        return this.herramienta_desarrollo;
    }

    public void setCodigo_programador(Integer codigo){
        this.codigo_programador = codigo;
    }

    public Integer getCodigo_programador(){
        return this.codigo_programador;
    }

    @Override
    public void obtener_actividades(){
        Collections.sort(actividades_diarias, Collections.reverseOrder());
        for(String otro:actividades_diarias){
            System.out.println(otro);
        }
    }

    public void eliminar_letras(){
        String letra = String.join("\n",actividades_diarias);

        String letra1 = letra.substring(1,25);
        String letra2 = letra.substring(28, 54);
        String letra3 = letra.substring(57, 85);
        
        System.out.println(letra1 + "\n" + letra2 + "\n" + letra3 + "\n");
        
    }    
}
