package Dia6Empleados;

public class Empleados {
    private String nombre;
    private String apellido;
    private Integer edad;
    private String actividades_diarias;
    private Integer codigo_empleado;

    public Empleados(String nombre, String apellido, Integer edad){
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getNombre(){
        return this.nombre;
    }

    public void setApellido(String apellido){
        this.apellido = apellido;
    }

    public String getApellido(){
        return this.apellido;
    }

    public void setEdad(Integer edad){
        this.edad = edad;
    }

    public Integer getEdad(){
        return this.edad;
    }

    public void setCodigo_empleado(Integer codigo){
        this.codigo_empleado = codigo;
    }

    public Integer getCodigo_empleado(){
        return this.codigo_empleado;
    }

}


